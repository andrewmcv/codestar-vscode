interface IGotoLocationCommand {
  file: string;
  pattern?: string;
}

interface IRetrieveLocationResponse {
  file: string;
  pattern?: string;
}
