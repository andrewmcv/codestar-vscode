// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import {
  ExtensionContext,
  commands,
  window,
  Uri,
  workspace,
  Position,
  Range,
  WindowState
} from "vscode";
import * as http from "http";

let newServer: http.Server;
let lastGotFocus = new Date();

function stringToNumber(text: string | undefined) {
  const val = Number(text);
  if (val.toString() !== text) {
    return undefined;
  }
  return val;
}

async function showInputBox(sucessCallback: (port: Number) => void) {
  const result = await window.showInputBox({
    value: "8081",
    prompt: "Codestar port?",
    validateInput: text => {
      return stringToNumber(text) === undefined ? "Must be a number" : null;
    }
  });
  sucessCallback(Number(result));
}

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: ExtensionContext) {
  const focused = window.state.focused;
  window.onDidChangeWindowState((e: WindowState) => {
    if (e.focused) {
      lastGotFocus = new Date();
    }
  });

  function run(port: Number) {
    // The code you place here will be executed every time your command is executed
    window.showInformationMessage("Codestar started on port " + port);

    const requestListener = (
      req: http.IncomingMessage,
      resp: http.ServerResponse
    ) => {
      switch (req.method) {
        case "GET":
          switch (req.url) {
            case "/getLocation":
              handleGetLocationCommand(resp);
              break;
            default:
              sendResponse(resp, 404, {
                error: "Cannot find command " + req.url
              });
              break;
          }
      }
      let data: any = [];
      req.on("data", chunk => {
        data.push(chunk);
      });

      req.on("end", () => {
        switch (req.method) {
          case "POST":
            handlePOST(data, req, resp);
            break;
          default:
            sendResponse(resp, 405, {
              error: "Cannot execute method " + req.method
            });
            break;
        }
      });
    };

    newServer = http.createServer(requestListener);
    newServer.listen(port);
  }

  let disposable = commands.registerCommand("extension.codestar", () => {
    showInputBox(run);
  });
  let disposableStop = commands.registerCommand(
    "extension.codestar-stop",
    () => {
      if (newServer && newServer.listening) {
        newServer.close();
        window.showInformationMessage("Codestar stopped");
      } else {
        window.showInformationMessage("Codestar not started");
      }
    }
  );
  context.subscriptions.push(disposable, disposableStop);
}

function handlePOST(
  data: any,
  req: http.IncomingMessage,
  resp: http.ServerResponse
) {
  try {
    const body = JSON.parse(data);

    switch (req.url) {
      case "/setLocation":
        const cmd = body as IGotoLocationCommand;
        openFile(cmd.file, cmd.pattern);
        sendResponse(resp, 200, { message: "successs" });
        break;
      default:
        sendResponse(resp, 404, {
          error: "Cannot execute command " + req.url
        });
        break;
    }
  } catch (err) {
    sendResponse(resp, 405, {
      error: "Problem handling POST, got an exception: " + err
    });
  }
}

function openFile(file: string, pattern?: string) {
  window.showTextDocument(Uri.file(workspace.rootPath + "/" + file)).then(
    () => {
      if (pattern) {
        let text = window.activeTextEditor!.document.getText();
        const line = findPatternLine(text, pattern);
        window.showTextDocument(Uri.file(workspace.rootPath + "/" + file), {
          selection: new Range(new Position(line, 0), new Position(line + 1, 0))
        });
      }
    },
    err => {}
  );
}

function handleGetLocationCommand(resp: http.ServerResponse) {
  let editor = window.activeTextEditor;
  if (editor) {
    let path = editor.document.uri.fsPath;
    let root = workspace.rootPath;
    if (root) {
      if (path.startsWith(root)) {
        path = path.substring(root.length + 1);
        const pattern = editor.document.getText(
          new Range(editor.selection.start, editor.selection.end)
        );
        sendResponse(resp, 200, {
          file: path,
          pattern: pattern,
          lastGotFocus
        });
      }
    }
  }
  // if we get here we have an issue or window is not currently focused
  sendResponse(resp, 404, {});
}

function sendResponse(resp: http.ServerResponse, status: number, obj: any) {
  resp.writeHead(status, { "Content-Type": "application/json" });
  resp.write(JSON.stringify(obj));
  resp.end();
}

function findPatternLine(text: string, pattern: string) {
  const lines = text.split("\n");
  let count = 0;
  for (const line of lines) {
    if (line.includes(pattern)) {
      return count;
    }
    count++;
  }
  return 0;
}

// this method is called when your extension is deactivated
export function deactivate() {
  newServer.close(function() {
    window.showInformationMessage("Codestar stopped");
  });
}
