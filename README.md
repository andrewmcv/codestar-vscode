# Codestar

Creates a small server in VSCode which allows the editor to be controlled via a UML case tool called StarUML. This supports diagrams of the code, allowing a programmer to quickly navigate around the codebase.

Here is the [corresponding extension](https://github.com/intrinsically/codestar-staruml) which you should also install in the [StarUML case tool](http://staruml.io/).

To start this run "Codestar: start" command & choose a port. E.g. if you choose port 8081, this will open up a small server on :8081 and you can execute the following commands:

    curl -X GET localhost:8081/getLocation

This returns a json structure with the current file and the selected pattern.

    {"file":"src/treetypes.ts","pattern":"foo"}

Post this back to set the location:

    curl -X POST -data @location.json localhost:8081/setLocation

You need to start Codestar in each new window with a different port. The StarUML extension by default talks to ports 8081 through to 8091, so it can control 10 different windows if required.
